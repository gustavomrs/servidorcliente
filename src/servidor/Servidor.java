package servidor;

import java.io.File;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Servidor implements Runnable {

    //Atributo Server Socket para podermos receber conexões de clientes
    private ServerSocket server;

    //Atributo booleano para sabermos se o servidor já foi inicializado ou não
    private boolean inicializado;

    //Atributo booleano para sabermos se o servidor está em execução ou não
    private boolean executando;

    //Lista de atendentes
    private List<Atendente> atendentes;

	//Atributo Thread para controlar e representar a thread auxiliar para
    //recebimento de conexões do servidor
    private Thread thread;
    
    //Caminho Servidor
    private final static String caminhoServidor = "C:/temp/files_to_be_served";

    private static Map<String, String> listaArquivos;

    public Servidor(int porta) throws Exception {
		// Método construtor para incializar o novo objeto Servidor
        //receberá como parâmetro a porta de serviço do servidor

        atendentes = new ArrayList<Atendente>();

        inicializado = false;
        executando = false;

        carregarListaArquivos();

        open(porta);

    }

    public void carregarListaArquivos() {
        listaArquivos = new HashMap<String, String>();

        File diretorio = new File(caminhoServidor);
        File arquivos[] = diretorio.listFiles();

        if (diretorio != null) {
            for (int i = 0; i < arquivos.length; i++) {
                listaArquivos.put(arquivos[i].getName(), caminhoServidor + arquivos[i].getName());
            }
        }
    }

    // Cria o objeto ServerSocket e marca o objeto servidor como inicializado	 
    private void open(int porta) throws Exception {
        server = new ServerSocket(porta);
        inicializado = true;
    }

    // Libera os recursos alocados pelo objeto servidor
    private void close() {

        for (Atendente atendente : atendentes) {

            try {
                atendente.stop();
            } catch (Exception e) {
                System.out.println(e);
            }
        }

        try {
            server.close();
        } catch (Exception e) {
            System.out.println(e);
        }

        server = null;

        inicializado = false;
        executando = false;

        thread = null;
    }

    //Inicia a execução do servidor
    public void start() {

        if (!inicializado || executando) {
            return;
        }

        executando = true;
        thread = new Thread(this);
        thread.start();
    }

    //Para a execuão do servidor
    public void stop() throws Exception {

        executando = false;

        if (thread != null) {
            thread.join();
        }

    }

    public void run() {

        System.out.println("Aguardando conexão.");

        while (executando) {
            try {

                //Se em 2500 ms não chegar outra conexão, uma exceção será lançada
                server.setSoTimeout(2500);

                Socket socket = server.accept();

                System.out.println("Conexão estabelecida");

                Atendente atendente = new Atendente(socket);
                atendente.start();

                atendentes.add(atendente);

            } catch (SocketTimeoutException e) {
                // ignorar
            } catch (Exception e) {
                System.out.println(e);
                break;
            }
        }

        close();
    }

    public static void main(String[] args) throws Exception {

        System.out.println("Iniciando Servidor");

        Servidor servidor = new Servidor(2525);
        servidor.start();

        System.out.println("Pressione ENTER para encerrar o servidor.");
        new Scanner(System.in).nextLine();

        System.out.println("Encerrando conexão.");

        servidor.stop();

    }

    public static Map<String, String> obterListaArquivos() {
        return listaArquivos;
    }

    public Map<String, String> getListaArquivos() {
        return listaArquivos;
    }

    public void setListaArquivos(Map<String, String> listaArquivos) {
        this.listaArquivos = listaArquivos;
    }

}
