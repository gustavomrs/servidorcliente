package servidor;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Atendente implements Runnable {

    private Socket socket;

    private BufferedReader in;
    private PrintStream out;

    private boolean inicializado;
    private boolean executando;

    private Thread thread;
    
    public final static String SEND_PATH_SERVER = "C:/temp/files_to_be_served/";

    public Atendente(Socket socket) throws Exception {
        this.socket = socket;

        this.inicializado = false;
        this.executando = false;

        open();

    }

    private void open() throws Exception {

        try {
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintStream(socket.getOutputStream());
            inicializado = true;
        } catch (Exception e) {
            close();
            throw e;
        }
    }

    public void start() {

        if (!inicializado || executando) {
            return;
        }

        executando = true;
        thread = new Thread(this);
        thread.start();
    }

    public void stop() throws Exception {
        executando = false;

        if (thread != null) {
            thread.join();
        }

    }

    private void close() {

        if (in != null) {

            try {
                in.close();
            } catch (Exception e) {
                System.out.println(e);
            }
        }

        if (out != null) {

            try {
                out.close();
            } catch (Exception e) {
                System.out.println(e);
            }
        }

        try {
            socket.close();
        } catch (Exception e) {
            System.out.println(e);
        }

        in = null;
        out = null;
        socket = null;

        inicializado = false;
        executando = false;

        thread = null;
    }

    public void sendMenuClient() {

        String menu = "";

        menu = "Escolha a opção desejada: \n"
                + "1)Listar arquivos \n"
                + "2)Baixar arquivos via TCP \n"
                + "3)Baixar arqruivos via UDP \n"
                + "4)Enviar arquivo \n"
                + "5)Encerrar comunicação: ";

        out.println(menu);
    }

    public void listarArquivosClient() {

        String listArquivos = "";

        Set<String> chaves = Servidor.obterListaArquivos().keySet();

        for (String chave : chaves) {
            if (chave != null) {
                listArquivos += chave + "\n";
            }
        }

        out.println(listArquivos);

    }

    public void baixarArquivoTcp(String mensagem) throws IOException {
        FileInputStream fis = null;
        BufferedInputStream bis = null;
        OutputStream os = null;
        String fileName =  (String) mensagem.subSequence(1, mensagem.length());
        
      // send file
          File myFile = new File (SEND_PATH_SERVER + fileName);
          byte [] mybytearray  = new byte [(int)myFile.length()];
          fis = new FileInputStream(myFile);
          bis = new BufferedInputStream(fis);
          bis.read(mybytearray,0,mybytearray.length);
          os = socket.getOutputStream();
          System.out.println("Sending " + SEND_PATH_SERVER + fileName + "(" + mybytearray.length + " bytes)");
          os.write(mybytearray,0,mybytearray.length);
          os.flush();
          System.out.println("Done.");
          
          if (bis != null) bis.close();
          if (os != null) os.close();
          if (socket != null) socket.close();
    }

    public void run() {

        while (true) {

            try {
                socket.setSoTimeout(2500);
                String mensagem = in.readLine();

                if ("0".equals(mensagem)) {
                    sendMenuClient();
                }

                if ("1".equals(mensagem)) {
                    listarArquivosClient();
                }

                if ("2".equals(mensagem.substring(0, 1))) {
                    baixarArquivoTcp(mensagem);
                }

//				System.out.println(
//				"Mensagem recebida do cliente [" +
//				socket.getInetAddress().getHostName() +
//				"]: " +
//				socket.getPort() +
//				":"+
//				mensagem);
                if ("FIM".equals(mensagem)) {
                    break;
                }

//				out.println(mensagem);
            } catch (SocketTimeoutException e) {
                //ignorar
            } catch (Exception e) {
                System.out.println(e);
                break;
            }

        }

        System.out.println("Encerrando conexão.");

        close();

    }
}
