package cliente;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Scanner;

public class Cliente implements Runnable {

    private Socket socket;

    private BufferedReader in;
    private PrintStream out;

    private boolean inicializado;
    private boolean executando;

    private Thread thread;
    private int opcao;
    
    public final static int FILE_SIZE = 6022386; //HARDCODED
    
    public final static String
       PATH_TO_RECEIVE = "C:/temp/served_files/";

    public Cliente(String endereco, int porta) throws Exception {
        inicializado = false;
        executando = false;

        opcao = 1;

        open(endereco, porta);
    }

    private void open(String endereco, int porta) throws Exception {

        try {
            socket = new Socket(endereco, porta);

            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintStream(socket.getOutputStream());

            inicializado = true;
        } catch (Exception e) {
            System.out.println(e);
            close();
            throw e;
        }

    }

    private void close() {
        if (in != null) {

            try {
                in.close();
            } catch (Exception e) {
                System.out.println(e);
            }
        }

        if (out != null) {

            try {
                out.close();
            } catch (Exception e) {
                System.out.println(e);
            }
        }

        if (socket != null) {

            try {
                socket.close();
            } catch (Exception e) {
                System.out.println(e);
            }
        }

        in = null;
        out = null;

        socket = null;

        inicializado = false;
        executando = false;

        thread = null;
    }

    public void start() {

        if (!inicializado || executando) {
            return;
        }

        executando = true;
        thread = new Thread(this);
        thread.start();
    }

    public void stop() throws Exception {

        executando = false;

        if (thread != null) {
            thread.join();
        }

    }

    public boolean isExecutando() {
        return executando;
    }

    public void send(String mensagem) {
        out.println(mensagem);
    }

    public void obterMenu() {
        out.println("0");
    }

    public void listArquivos() {
        out.println("1");
    }

    public void baixarArquivoTcp(String arquivo) throws IOException {
        out.println("2" + arquivo);

        int bytesRead;
        int current = 0;
        FileOutputStream fos = null;
        BufferedOutputStream bos = null;
        
        //recebendo arquivo
        byte[] mybytearray = new byte[FILE_SIZE];
        InputStream is = socket.getInputStream();
        fos = new FileOutputStream(PATH_TO_RECEIVE + arquivo);
        bos = new BufferedOutputStream(fos);
        bytesRead = is.read(mybytearray, 0, mybytearray.length);
        current = bytesRead;

        do {
            bytesRead
                    = is.read(mybytearray, current, (mybytearray.length - current));
            if (bytesRead >= 0) {
                current += bytesRead;
            }
        } while (bytesRead > -1);

        bos.write(mybytearray, 0, current);
        bos.flush();
        System.out.println("File " + PATH_TO_RECEIVE + arquivo
                + " downloaded (" + current + " bytes read)");
        
        if (fos != null) fos.close();
        if (bos != null) bos.close();
        if (socket != null) socket.close();
    }
    
    public void run() {
        while (executando) {
            try {
//        socket.setSoTimeout(2500);

                String mensagem = in.readLine();

                if (mensagem == null) {
                    break;
                }

                System.out.println(
                        mensagem);
            } catch (SocketTimeoutException e) {
                //ignorar
            } catch (Exception e) {
                System.out.println(e);
                break;
            }
        }

        close();
    }

    public static void main(String[] args) throws Exception {

        System.out.println("Inicializando Cliente ...");

        System.out.println("Iniciando conexão com o servidor ...");

        Cliente cliente = new Cliente("localhost", 2525);

        System.out.println("Conexão estabelecida com sucesso ...");

        cliente.start();

        Scanner scanner = new Scanner(System.in);

        cliente.obterMenu();

        while (true) {

            String mensagem = scanner.nextLine();

            if (!cliente.isExecutando()) {
                break;
            }

            if ("1".equals(mensagem)) {
                cliente.listArquivos();
            }

            if ("2".equals(mensagem)) {
                System.out.println("\nInforme o nome do arquivo que deseja baixar. \n");
                String arq = scanner.nextLine();
                cliente.baixarArquivoTcp(arq);
                break;
            }

//      cliente.send(mensagem);
            if ("5".equals(mensagem)) {
                break;
            }

            cliente.obterMenu();

        }

        System.out.println("Encerrando cliente ...");

        cliente.stop();
    }

}
